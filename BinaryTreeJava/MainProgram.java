package BinaryTreeJava;
import QueueJava.Queue;
import java.util.Random;
import java.io.*;
public class MainProgram{
	public static void main(String[] args){
		Random rand = new Random();
		Queue<Integer> queue = new Queue<Integer>();
		/*for(int x = 0; x<rand.nextInt(1000); x++)
			queue.enqueue(rand.nextInt(5000));*/
		queue.enqueue(10);
		queue.enqueue(6);
		queue.enqueue(15);
		queue.enqueue(4);
		queue.enqueue(8);
		queue.enqueue(12);
		queue.enqueue(18);
		queue.enqueue(27);

		Tree<Integer> tree = new Tree<Integer>(null);
		tree.createBinaryTree(tree,queue);
		System.out.println(tree.left().left().left().read());
		
		//tree.bfs();
	}
}
