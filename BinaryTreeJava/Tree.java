package BinaryTreeJava;
import java.io.*;
import QueueJava.Queue;
public class Tree<type>{
	private Tree parent;
	private Tree<type> left;
	private Tree<type> right;
	private type data;

	public Tree(type data){
		this.parent = this.left = this.right = null;
		this.data = data;
	}	

	public type read() {return this.data;}
	public void write(type data) {this.data = data;}
	public Tree parent() {return this.parent;}
	public Tree left() {return this.left;}
	public Tree right() {return this.right;}
	public void insertLeft(Tree<type> tree) {tree.setParent(this);this.left = tree;}
	public void insertRight(Tree<type> tree) {tree.setParent(this);this.right = tree;}
	public void deleteLeft() {this.left = null;}
	public void deleteRight() {this.right = null;}
	public void setParent(Tree tree) {this.parent = tree;}

	public void createBinaryTree(Tree<type> root, Queue<type> data){ //a stack is used to memorize data to create Tree structure
		Queue<Tree> priority = new Queue<Tree>();
		root.write(data.dequeue());
		root.insertLeft(new Tree<type>(null));
		priority.enqueue(root.left());
		root.insertRight(new Tree<type>(null));
		priority.enqueue(root.right());
		while(!priority.isEmpty() && data.length > 1){
			Tree left = priority.dequeue();
			((Tree<type>)left).write(data.dequeue());
			Tree right = priority.dequeue();
			((Tree<type>)right).write(data.dequeue());
			left.insertLeft(new Tree<type>(null));
			priority.enqueue(left.left());
			left.insertRight(new Tree<type>(null));
			priority.enqueue(left.right());
			right.insertLeft(new Tree<type>(null));
			priority.enqueue(right.left());
			right.insertRight(new Tree<type>(null));
			priority.enqueue(right.right());
		}
		if(!data.isEmpty() && !priority.isEmpty())
			priority.dequeue().write(data.dequeue());
		
		
	}
	
	public void preOrder(Tree root){
		if(root != null){
			System.out.print(root.read()+" ");
			preOrder(root.left());
			preOrder(root.right());
		}
	}
	public void inOrder(Tree root){
		if(root != null){
			preOrder(root.left());
			System.out.print(root.read()+" ");
			preOrder(root.right());
		}
	}
	public void postOrder(Tree root){
		if(root != null){
			preOrder(root.left());
			preOrder(root.right());
			System.out.print(root.read()+" ");
		}
	}
	public void bfs(){
		Queue<Tree> queue = new Queue<Tree>();
		queue.enqueue(this);
		while(!queue.isEmpty()){
			Tree tree = queue.dequeue();
			System.out.print(tree.read()+" ");
			if(tree.left() != null)
				queue.enqueue(tree.left());
			if(tree.right() != null)
				queue.enqueue(tree.right());
		}
	}

}
