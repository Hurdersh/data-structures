#include "Stack"
#include<typeinfo>
#include<exception>
#include<iostream>
template<class T>
Stack<T>::Stack(int dimension){
	if((int) dimension != dimension)
		throw std::invalid_argument("insert an int dimension");
	this->dimension = dimension;
	this->array = new T[dimension];
	index = 0;
}

template<class T>
void Stack<T>::dequeue(){
	if(index > 0) {//if i have 1 element or more
		this->array[index-1] = '\0';
		index--;
	}
	else
		throw std::invalid_argument("nothing in the Stack");
}

template<class T>
void Stack<T>::enqueue(T data){
	if(typeid(data).name() != typeid(T).name())
		throw std::invalid_argument("not the correct type");
	if(index > dimension-1)
		throw std::out_of_range("out of range");
	this->array[index] = data;
	index++;
}

template<class T> //specialized for string because c++ don't convert string to string WTF
std::string Stack<T>::toString(){
	std::string s = "";
	for(int x = 0;x<index;x++){
		s+=std::to_string(this->array[x]);
		s+=" ";
	}
	return s;
}


template<> //specialized for string because c++ don't convert string to string WTF
std::string Stack<std::string>::toString(){
	std::string s = "";
	for(int x = 0;x<index;x++){
		s+=this->array[x];
		s+=" ";
	}
	return s;
}

