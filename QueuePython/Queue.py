#!/usr/bin/env python3
from Data import Data
class Queue:
    def __init__(self):
        self.head = None
        self.tail = None

    def isEmpty(self):
        return self.head is None

    def enqueue(self, data):
        d = Data(data)
        if self.isEmpty():
            self.head = self.tail = d
        else:
            self.tail.__setattr__('next', d)
            d.__setattr__('previous', self.tail)
            self.tail = d

    def dequeque(self):
        if self.isEmpty():
            raise IndexError("empty queue")
        if self.head.__getattribute__('next') is not None:
            self.head.__getattribute__('next').__setattr__('previous', None)
        self.head = self.head.__getattribute__('next')

    def display(self):
        node = self.head
        while node is not None:
            print(f"{node.__getattribute__('data')} ", end="")
            node = node.__getattribute__('next')


