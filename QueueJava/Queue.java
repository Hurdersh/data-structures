package QueueJava;
//this is a queue impemented in java as a Doubly linked list
public class Queue<type>{
	private Node<type> head;
	private Node<type> tail;
	public Integer length;
	public Queue(){
		this.head = this.tail = null;
		this.length = 0;
	}
	
	public boolean isEmpty(){return head==null;}
	public void enqueue(type data){
	       Node<type> node = new Node<type>(data);
	       if(head == null){
		       head = tail = node;
	       }
	       else{
		       node.setNext(head);
		       head.setPrevious(node);
		       head = node;
	       }
	       length+=1;
	}
	public type dequeue(){
		Node<type> node = tail;
		if(head == null){
			throw new NullPointerException();
		}
		else if(head == tail){
			head = tail = null;	
		}
		else{
			tail.getPrevious().setNext(null);
			tail = tail.getPrevious();
		}
		length-=1;
		return node.getData();
	}

	public void display(){
		if(head == null){throw new NullPointerException();}
		//print in reverse order cause Queue delete tail first
		Node<type> node = tail;
		while(node != null){
			System.out.println(node.getData());
			node = node.getPrevious();
		}

	}	
}
