package QueueJava;
//this is a node for queue implemented as a simple linked list
public class Node<type>{
	private Node<type> next;
	private Node<type> previous;
	private type data;
	public Node(type data){
		this.data = data;
		this.next = this.previous = null;
	}
	public Node<type> getNext() {return this.next;}
	public Node<type> getPrevious() {return this.previous;}
	public type getData() {return this.data;}
	public void setNext(Node<type> node) {this.next = node;}
	public void setPrevious(Node<type> node) {this.previous = node;}
}	
