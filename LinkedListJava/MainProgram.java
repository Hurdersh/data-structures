package LinkedListJava;
public class MainProgram{
	public static void main(String[] args){
		DoubleLinkedList list = new DoubleLinkedList();	
		list.insertTail("hello",12);
		list.insertTail("centerba",87);
		list.remove(12);
		list.add(87,"until",5);
		list.insertTail("toro",2);
		list.remove(5);
		list.display();
	}
}
