package LinkedListJava;
public class Node{
	private Node next; //next and previous are nodes of concateneted list
	private Node previous;
	private String data;
	private Integer identifier; // is used as identifier for insert and remove

	public Node(String data, Integer identifier){
		this.next = this.previous = null;
		this.data = data;
		this.identifier = identifier;
	}
	//setter and getter methods according to java standards
	public void setNext(Node next) {this.next = next;}
	public void setPrevious(Node previous) {this.previous = previous;}
	public Node getNext() {return this.next;}
	public Node getPrevious() {return this.previous;}
	public Integer getIdentifier() {return this.identifier;}

	//method used only for visualization
	public String toString() {return this.data + " " + this.identifier;}

}
