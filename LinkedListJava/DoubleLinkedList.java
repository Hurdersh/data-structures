package LinkedListJava;
public class DoubleLinkedList {
	private Node head; //head and tail are the first and the last nodes
	private Node tail;

	public DoubleLinkedList(){
		this.head = this.tail = null;
	}
	private Node search(Integer n){ //everytime i'll work on a medium value complexity of algorithm is O(n)
		Node  node = head;
		while(node != null && node.getIdentifier() != n){
			node = node.getNext();
		}
		if(node == null)
			throw new NullPointerException();
		else
			return node;

	}
	private boolean checkData(String data, Integer identifier){
		if(data != null && identifier != null && identifier == (int) identifier) return true;
		return false;
	}

	//implementation(methods) are written in the Cormen Algorithm book
	public boolean isEmpty(){return head == null;} //if head is null array is null also

	public Node head(){
		if(head == null)
			throw new NullPointerException("list is empty");
		return head;
	}

	public Node tail(){
		if(head == null)
			throw new NullPointerException("list is empty");
		return tail; 
	}

	public Node next(Integer identifier){
		Node node = this.search(identifier);
		if(node.getNext() == null){
			throw new ArrayIndexOutOfBoundsException("that's the tail");
		}
		return node.getNext();
	}

	public Node previous(Integer identifier){
		Node node = this.search(identifier);
		if(node.getPrevious() == null){
			throw new ArrayIndexOutOfBoundsException("that's the head");
		}
		return node.getPrevious();
	}

	public Node nextHead(){
		if(this.isEmpty()){
			throw new NullPointerException("array is empty");
		}
		if(head.getNext() == null){
			throw new ArrayIndexOutOfBoundsException("head hasn't next");
		}
		return head.getNext();
	}
	
	public Node previousTail(){
		if(this.isEmpty()) throw new NullPointerException("array is empty");
		if(tail.getPrevious() == null) throw new ArrayIndexOutOfBoundsException("head and tail are the same");
		return tail.getPrevious();
	}

	//these 2 methods are the focus of the data structure, i'm going to insert with time complexity of O(1)
	public void insertHead(String data, Integer identifier){
		if(this.isEmpty()){
			head = tail = new Node(data,identifier);
		}else{
			Node node = new Node(data,identifier);
			node.setNext(head);
			head.setPrevious(node);
			head = node;
		}
	}

	public void insertTail(String data, Integer identifier){
		if(this.isEmpty()){
			head = tail = new Node(data,identifier);
		}else{
			Node node = new Node(data,identifier);
			node.setPrevious(tail);
			tail.setNext(node);
			tail = node;
		}
	}

	//add at specified position with complexity O(n)
	public void add(Integer n,String data, Integer identifier){
		Node insert = this.search(n);
		if(insert == null){
			throw new NullPointerException("this node doesn't exists");
		}else{
			Node node = new Node(data,identifier);
			node.setPrevious(insert);
			node.setNext(insert.getNext());
			if(insert.getNext() != null){
				insert.getNext().setPrevious(node);
			}
			if((insert == tail && insert == head)||insert == tail){ //if i'm going to insert after the tail i must become the new tail
				insert.setNext(node);
				tail = node;
			}
			else{
				insert.setNext(node);
			}
		}
	}

	public void remove(Integer n){
		Node rem = this.search(n);
		//i will remove only if i find this node
		if(rem != null){
			if(rem == head && rem == tail){
				head = tail = null;
			}
			if(rem.getPrevious() != null){
				rem.getPrevious().setNext(rem.getNext());
			}else{
				head = rem.getNext();
			}
			if(rem.getNext() != null){
				rem.getNext().setPrevious(rem.getPrevious());
			}else{
				tail = rem.getPrevious();
			}
		}
	}

	public void display(){
		Node node = head;
		while(node != null){ 
			System.out.println(node.toString());
			node = node.getNext();
		}
	}

}
